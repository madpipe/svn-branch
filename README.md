# svn-branch
This is a tool that helps ease SVN branch management.

Usage:
```
./main.py settings.json
```
This prints out the available branches and asks to whick one to switch.
The option to create a new branch is not yet implemented.

Untill this get's implemented in the script itself, this shell function can be very useful:
```
function svnbranch() {
	if [ -e ./settings.json ]; then
		/path/to/svn-branch/main.py ./settings.json
	elif [ -n "$1" ]; then
		/path/to/svn-branch/main.py $1
	else
		echo "No settings file specified. Please specify a settings file."
		return 1
	fi
}
```

