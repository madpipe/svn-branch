#!/usr/bin/env python3

import argparse

argParser = argparse.ArgumentParser()
argParser.add_argument("settingsFile", help="path to the settings file")
argParser.add_argument("-v", "--verbose")
args = argParser.parse_args()

projectName = ""
projectPath = "" # location on the filesystem of the project
trunkPath   = "" # SVN TRUNK location of the project
branchPath  = "" # SVN BRANCH location of the project

existingBranches = { 1: "TRUNK" }
currentBranch = ""

import os

#if file exists:
if os.path.exists(args.settingsFile):
	import json
	with open(args.settingsFile, 'r') as jsonFile:
		jsonData = json.load(jsonFile)
	projectName = jsonData['name']
	projectPath = jsonData['path']
	trunkPath   = jsonData['trunk']
	branchPath  = jsonData['branch']

# get existing branches
print("Getting available branches...")
import subprocess
command = ['svn', 'list', '--xml', branchPath]
branchXml = subprocess.check_output(command)

import xml.etree.ElementTree as ET
root = ET.fromstring(branchXml)
counter = 2
for child in root[0]:
	if child.attrib.get('kind') == "dir":
		existingBranches[counter] = child[0].text
		counter += 1

# get current branch
def getCurrentBranch():
	global currentBranch
	command = ['svn', 'info', '--xml', projectPath]
	infoXml = subprocess.check_output(command)
	root = ET.fromstring(infoXml)
	for child in root.findall('entry'):
		currentBranch = child.find("url").text
		currentBranch = currentBranch.replace(branchPath, "")
	if "/TRUNK/" in currentBranch:
		currentBranch = "TRUNK"

def getOption(message, options):
	message += "\n"
	line = False
	for index, option in options.items():
		if str(index).isdigit():
			message += str(index) + "\t" + option + "\n"
		else:
			if not line:
				message += "\n"
				line = True
			message += index + "\t" + option + "\n"
	print(message)
	attempts = 0
	while attempts < 3:
		option = -1
		option = input("--> ")
		if option.isdigit():
			option = int(option)
		if option not in options.keys():
			print("Invalid option. Try another.")
			attempts += 1
			continue
		return option
	return -1

def isWcClean():
	print("Checking if working copy is clean...")
	command = ["svn", "status", "-q", projectPath]
	output = subprocess.check_output(command)
	if len(output) > 0:
		return False
	return True

def switchBranch(branch):
	if branch == "TRUNK":
		branch = trunkPath
	else:
		branch = branchPath + branch
	command = ["svn", "switch", branch, projectPath]
	if args.verbose:
		subprocess.call(command)
	else:
		subprocess.check_output(command)
	getCurrentBranch()
	print("Now on branch: " + currentBranch)


# Choose branch message composition
getCurrentBranch()
message = "Chose branch to switch to.\n"
options = existingBranches.copy()
for index, branch in existingBranches.items():
	if branch == currentBranch:
		options[index] = "*" + branch
	else:
		options[index] = " " + branch
options["n"] = "Create new branch (from current branch)"
options["q"] = "Quit"

option = getOption(message, options)
if option == 'q':
	exit(0)
if option == 'n':
	while True:
		newBranchName = input("Enter new branch name: ")
		import re
		if re.fullmatch("[\w \-_.]*", newBranchName):
			break
		else:
			print("Invalid branch name: " + newBranchName)
	if not isWcClean():
		x = input("Working copy is not clean, continue? [Y/n]")
		if x != "" and x != "y" and x != "Y":
			exit(0)
	print("creating branch new branch \"" + newBranchName + "\"...")
	branch = branchPath + currentBranch
	if currentBranch == "TRUNK":
		branch = trunkPath
	command = ["svn", "copy", branch, branchPath + newBranchName]
	subprocess.call(command)
	x = input("Switch to the new branch? [Y/n]")
	if x == "" or x == "y" or x == "Y":
		switchBranch(newBranchName)
	exit(0)
try:
	option = int(option)
except ValueError as e:
	print("Invalid input.")
	exit(1)

for index, branch in existingBranches.items():
	if option == index:
		if branch == currentBranch:
			print("Current branch selected. Nothing to do.")
			exit(0)
		if not isWcClean():
			print("Working copy is not clean. Cannot switch branch.")
			exit(2)
		print("Chosen branch " + branch + ". Switching...")
		switchBranch(branch)

